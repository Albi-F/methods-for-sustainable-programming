"""Unit testing for the player class."""

from typing import List
import unittest
import re
from src.pig import player


class TestPlayerClass(unittest.TestCase):
    """Test the class."""

    def test_init_default_object(self) -> None:
        """Test the initial state of the player class."""
        player1_name: str = "The Champion"

        player1: player.Player = player.Player(player1_name)
        self.assertIsInstance(player1, player.Player)

        self.assertEqual(player1.name, player1_name)

        expected_temporary_points: int = 0
        self.assertEqual(player1.temporary_points, expected_temporary_points)

        expected_total_points: int = 0
        self.assertEqual(player1.total_points, expected_total_points)

    def test_dice_rolled(self) -> None:
        """Test the saving of the die."""
        dice_to_roll: List[List[int]] = [[3, 2, 5, 4], [2, 1], [1], [5]]

        player1: player.Player = player.Player("The one")

        counter: int = 0
        while True:
            for x in dice_to_roll[counter]:
                player1.save_roll(x)
            if counter == len(dice_to_roll)-1:
                break
            else:
                player1.end_turn()
                counter += 1

        self.assertEqual(player1.dice_rolled, dice_to_roll)

        expected_temporary_points: int = 5
        self.assertEqual(player1.temporary_points, expected_temporary_points)

        expected_total_points: int = 14
        self.assertEqual(player1.total_points, expected_total_points)

    def test_string_representation(self) -> None:
        """Test the __str__ standard method."""
        player1_name: str = "No way"

        player1: player.Player = player.Player(player1_name)

        self.assertRegex(player1.__str__(), re.escape(player1_name))
        self.assertRegex(player1.__str__(), re.escape(str(player1.id)))


if __name__ == '__main__':
    unittest.main()
