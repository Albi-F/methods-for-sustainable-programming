"""This module is used for the I/O."""
import cmd
import time
from typing import Optional, Any
from src.pig.game import Game
from src.pig.highscore import Highscore


class Shell(cmd.Cmd):
    """Simple command processor example."""

    def __init__(self) -> None:
        """Initialize cmd class variables."""
        cmd.Cmd.__init__(self)

        self.intro = """
Let's play a dice game called "Pig".

You can play this game against another person
or against your computer.

The purpose of this game is to accumulate
as many points as possible.

Type 'help' to see the commands.
"""
        self.game_prompt = "(Pig) "
        self.current_game: Optional[Game] = None

        self.ruler = "-"
        self.prompt = self.game_prompt

    # ---- SUPERCLASS METHODS ----
    def default(self, line: str) -> bool:
        """Print message wrong command."""
        print("The command specified is not valid, tye 'help' for a list of valid commands")
        return False

    def emptyline(self) -> bool:
        """If the user does not insert any command do the same as a wrong command."""
        return self.default("")

    def postcmd(self, stop: Any, line: str) -> Any:
        """Change the prompt depending on the state of the game."""
        if self.current_game is not None:
            self.prompt = self.current_game.current_player.name + "--> "
        else:
            self.prompt = "(Pig) "
        return stop

    # ---- DO METHODS ----
    # changename methods
    def do_changename(self, line: str) -> None:
        """Choose a new name."""
        if self.current_game is None:
            self.no_game_started()
            return
        if not line:
            print("Name not changed, the name should be at least one character long")
            return
        self.current_game.change_player_name(line)

    def help_changename(self) -> None:
        """Help function for the changename method."""
        print("Change your name")
        print("syntax: changename [name]")
    ####

    def do_cheat(self, line: str) -> None:
        """Cheat mode (for debugging)."""
        if self.current_game is None:
            cheater = "Cheater"
            self.current_game = Game(cheater, "Computer")
        else:
            print("Not possible, a game is in progress")

    def do_endturn(self, line: str) -> None:
        """End your turn."""
        if self.current_game is None:
            self.no_game_started()
            return
        print("You ended your turn")
        self.ending_turn()

    def do_exit(self, line: str) -> bool:
        """Close the program."""
        reply = input("Are you sure you want to close the program (y)? ")
        if reply == 'y':
            return True
        return False

    def do_highscore(self, line: str) -> None:
        """See statistics."""
        print(Highscore().print_high_scores())

    # newgame methods
    def do_newgame(self, line: str) -> None:
        """Start a new game."""
        if self.current_game is not None:
            print("Quit the current game before starting a new one")
            return
        if not line:
            print("Please check the command syntax with 'help newgame'")
            return
        players = line.split()
        if (len(players) == 1):
            self.current_game = Game(players[0], "Computer")
        else:
            self.current_game = Game(players[0], players[1])

    def help_newgame(self) -> None:
        """Help function for the newname method."""
        print("Start a new game")
        print("To play against the computer insert only player 1 name")
        print("To play against a friend insert both names")
        print("syntax: newgame [player1 name] [player2 name]")
    ####

    def do_points(self, line: str) -> None:
        """Show your points."""
        if self.current_game is None:
            self.no_game_started()
            return
        self.print_points()

    def do_quit(self, line: str) -> None:
        """Quit the current game."""
        if self.current_game is not None:
            self.stop_game()

    def do_roll(self, line: str) -> None:
        """Roll the die."""
        if self.current_game is None:
            self.no_game_started()
            return
        msg = ("It is a {}.")
        die_value: int = self.current_game.player_roll()
        print(msg.format(die_value))
        if die_value == 1:
            self.ending_turn()

    # ---- AUXILIARY METHODS ----
    def computer_turn(self) -> None:
        """Manage computer's round."""
        if self.current_game is None:
            return
        print("Computer's turn")
        die_value: int = 0
        while (
            die_value != 1 and
            self.current_game.current_player.temporary_points < 12
        ):
            time.sleep(1)
            die_value = self.current_game.player_roll()
            print("The computer rolled a " + str(die_value))
        self.ending_turn()

    def ending_turn(self) -> None:
        """Logic to manage the end of the turn."""
        if self.current_game is None:
            return
        victory: bool = self.current_game.end_player_turn()
        self.print_points()
        if victory:
            print("Victory!!!")
            if self.current_game.current_player.name != "Cheater":
                Highscore().add_name_score(self.current_game.current_player.name,
                                           self.current_game.current_player.id,
                                           self.current_game.current_player.dice_rolled)
            self.stop_game()
        else:
            self.current_game.next_player_turn()
            if self.current_game.current_player.name == "Computer":
                self.computer_turn()

    def no_game_started(self) -> None:
        """Error message for having called a method that requires an ongoing match."""
        print("You can not use this command because you are not playing a match/n")

    def print_points(self) -> None:
        """Print the points of the actual player."""
        if self.current_game is None:
            return
        temp_points, total_points = self.current_game.player_points()
        print("temporary points: " + str(temp_points))
        print("total points: " + str(total_points))

    def stop_game(self) -> None:
        """Stop the current match."""
        self.current_game = None
