"""This module manage the game."""
import itertools
from typing import List
from typing import Iterator
import random

from src.pig.player import Player


class Game():
    """Create and interact with the player class."""

    die_faces: int = 6
    points_for_victory: int = 100

    def __init__(self, player1_name: str, player2_name: str):
        """Set the initial points of the player."""
        self.players: List[Player] = [Player(player1_name), Player(player2_name)]
        self.players_iterator: Iterator[Player] = itertools.cycle(self.players)
        self.current_player: Player = next(self.players_iterator)

    def change_player_name(self, new_name: str) -> None:
        """Change player name inside player class."""
        self.current_player.name = new_name

    def next_player_turn(self) -> None:
        """Change player."""
        self.current_player = next(self.players_iterator)

    def end_player_turn(self) -> bool:
        """End player turn."""
        self.current_player.end_turn()
        if self.current_player.total_points >= 100:
            return True
        elif self.current_player.name == "Cheater" and self.current_player.total_points >= 20:
            return True
        return False

    def player_roll(self) -> int:
        """Ask player to roll die."""
        die_value = random.randint(1, Game.die_faces)
        self.current_player.save_roll(die_value)
        return die_value

    def player_points(self) -> tuple[int, int]:
        """Retrieve player points."""
        return self.current_player.temporary_points, self.current_player.total_points
