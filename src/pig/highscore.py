"""This class manage the high scores board."""
from typing import Dict, List, Union
from operator import itemgetter
from tabulate import tabulate


class Highscore():
    """Highscore class."""

    def __init__(self) -> None:
        """Initialize scores' list."""
        self.high_scores: List[Dict[str, Union[str, int]]] = []
        self.high_scores_list_lenght: int = 4

    def add_name_score(self, name: str, id: int, rolled_dice: List[List[int]]) -> None:
        """Append name string and score int to list."""
        best_turn_score: int = 0
        for turn in rolled_dice:
            turn_score: int = 0
            if 1 not in turn:
                turn_score = sum(turn)
            best_turn_score = max(turn_score, best_turn_score)
        turns: int = len(rolled_dice) - 1
        score_entry: Dict[str, Union[str, int]] = {
                                                   "Name": name,
                                                   "ID": id,
                                                   "Turns played": turns,
                                                   "Best turn score": best_turn_score
                                                  }
        self.high_scores.append(score_entry)
        self.high_scores.sort(key=itemgetter("Best turn score"), reverse=True)

    def print_high_scores(self) -> str:
        """Sort list and print out highscore."""
        if not self.high_scores:
            return "No score saved"
        return tabulate(self.high_scores, headers="keys")
