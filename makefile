PYTHON = python

.PHONY: pydoc

all:

clean:
	# Compiled Python Files
	find . -type f -name "*.py[co]" -delete -o -type d -name __pycache__ -delete
	# HTML coverage
	rm -rf htmlcov
	# flake8, coverage reports
	rm -f *report.xml
	# log files
	find . -type f -name "*.log" -delete
	# mypy cache
	rm -rf .mypy_cache
	# HTML documentation
	rm -rf doc
	# pyreverse DOT files
	find . -type f -name "*.dot" -delete
	# coverage file
	rm -f .coverage

coverage:
	coverage run -m unittest
	coverage report -m
	
coverage-html: coverage
	coverage html

coverage-xml: coverage
	coverage xml

lint:
	flake8
	mypy "src"

pdoc:
	pdoc --html --force --output-dir doc/pdoc .

doc: pdoc pyreverse

pyreverse:
	# DOT is a graph description language.
	# DOT graphs are typically files with the filename extension gv or dot.
	# install command is used to copy files and set attributes
	install -d doc/pyreverse
	pyreverse src/
	dot -Tpng classes.dot -o doc/pyreverse/classes.png
	dot -Tpng packages.dot -o doc/pyreverse/packages.png
	rm -f classes.dot packages.dot
	ls -l doc/pyreverse

# gitpod:
#	export PYTHONPATH=":/workspace"
# NOT POSSIBLE
# If you want the environment variables to be exported to the shell from which you invoked make things are difficult because you
# cannot directly export environment variables from a child process (make) to its parent process (the shell from which you invoke make).